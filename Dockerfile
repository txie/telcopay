FROM ubuntu:16.04
RUN apt-get update && apt-get install -y \
    software-properties-common \
    build-essential \
    openssl \
    libssl-dev \
    vim \
    git \
    curl \
    wget \
    unzip \
    gcc \
    g++ \
    make \
    nginx
 
RUN add-apt-repository -y ppa:ethereum/ethereum
RUN apt-get update && apt-get install -y \
    ethereum

RUN \ 
    (curl -sL https://deb.nodesource.com/setup_9.x -o nodesource_setup.sh && bash nodesource_setup.sh) && \
    apt-get install -y nodejs

# Install success, run remix-ide failed
RUN \ 
    mkdir /usr/remix-ide && cd /usr/remix-ide && npm install remix-ide

RUN \
    (cd /usr && git clone https://github.com/carsenk/explorer.git) && \
    (cd /usr/explorer && npm install)
# RUN npm start

WORKDIR /usr/telcopay
RUN \
    (mkdir telcopay-eth && cd telcopay-eth && npm install web3) && \
    (curl https://gitlab.com/txie/telcopay/-/archive/v0.1/telcopay-v0.1.tar | tar x --strip-components=2 -C /var/www/html)

# Telcopay web
EXPOSE 80

# Ethereum Explorer
EXPOSE 8000

# remix-ide
EXPOSE 8080

# geth 
EXPOSE 8545

# CMD exec nginx -g "daemon off;"
# CMD exec (geth --dev --datadir "~/work/txie-ethdev01" --rpc --rpccorsdomain "*" console)
# CMD exec (remix-ide)
# CMD exec (cd /usr/explorer && npm start)

# ENTRYPOINT ["/go/src/github.com/iotexproject/iotex-core/tools/start_node.sh"]
# RUN set -x \
#     && geth --dev --datadir "~/work/txie-ethdev01" --rpc --rpccorsdomain "*" \
#     && cd /usr/explorer && npm start \
#     && nginx -g "daemon off;"

# ENTRYPOINT [ "/usr/telcopay/start_telcopay.sh" ]


