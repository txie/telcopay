var accounts = {
    'alice': {
        firstName: 'Alice',
        lastName: 'Jones',
        emailAddress: 'ajones@telcopay.com',
        phoneNumber: '(650) 430-9266',
        password: 'test', 
        carrierName: "att",
        account: '0x27e947b0c0353301930151f6b708dd5686934a89'                
    },

    'bob': {
        firstName: 'Bob',
        lastName: 'Green',
        password: 'test',
        emailAddress: 'bgreen@telcopay.com',
        phoneNumber: '(650) 430-9266',
        carrierName: "verizon",
        account: '0xc380ba85967c55266e9a5bfc7c8a528f5b721d6f'
    }, 

    'xiaohong': {
        firstName: 'Xiaohong',
        lastName: 'Liu',
        password: 'test',
        emailAddress: 'xliu@telcopay.com',
        phoneNumber: '189-3560988',
        carrierName: "china-telecom",
        account: '0x03a3b951bCe4aA46Be3577D889b17583E63b9Ef3'
    },

    'jose': {
        firstName: 'Jose',
        lastName: 'Gomez',
        password: 'test',
        emailAddress: 'jgomez@telcopay.com',
        phoneNumber: '(632) 816-9266',
        carrierName: "pldt",
        account: '0xFeeb906CDDEDf8FA62E31318e1587033CD612abd'
    },

    'charlie': {
        firstName: 'Charlie',
        lastName: 'Klein',
        emailAddress: 'cklein@telcopay.com',
        phoneNumber: '(650) 410-3011',
        password: 'test',
        account: '0x27e947b0c0353301930151f6b708dd5686934a89'                
    },
    'amy': {
        firstName: 'Amy',
        lastName: 'Jackson',
        password: 'test',
        emailAddress: 'amy@telcopay.com',
        phoneNumber: '(510) 450-3013',
        account: '0xc380ba85967c55266e9a5bfc7c8a528f5b721d6f',
    } 
}

var meta = {
    'corpName': 'China Telecom',
    'corpLogo': 'china-telecom-logo.png',
    'pldt': 'pldt-logo.png'
}

const devnet_url = "http://localhost:7545";

const transTokenContractAddr = '0x31f5a55dcbaf9fdebbc1fdd019d08bffc178af7a';
const telcoMarketContractAddr = '0xcedb5f0454edd5b9c882f263f73a9bbd1ea08d35';
const transTokenContractContent = [{"constant":true,"inputs":[],"name":"getMyEthBalance","outputs":[{"name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[{"name":"receiver","type":"address"},{"name":"amount","type":"uint256"}],"name":"sendToken","outputs":[{"name":"sufficient","type":"bool"}],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[],"name":"getMyBalance","outputs":[{"name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[{"name":"act","type":"address"}],"name":"getEthBalance","outputs":[{"name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[],"name":"setEthValue","outputs":[],"payable":true,"stateMutability":"payable","type":"function"},{"constant":true,"inputs":[],"name":"symbol","outputs":[{"name":"","type":"string"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[{"name":"","type":"address"}],"name":"tokenBalanceOf","outputs":[{"name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[{"name":"act","type":"address"}],"name":"getBalance","outputs":[{"name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"inputs":[{"name":"_symbol","type":"string"},{"name":"supply","type":"uint256"}],"payable":false,"stateMutability":"nonpayable","type":"constructor"},{"anonymous":false,"inputs":[{"indexed":false,"name":"sender","type":"address"},{"indexed":false,"name":"receiver","type":"address"},{"indexed":false,"name":"amount","type":"uint256"}],"name":"TokenTransfer","type":"event"}];

// const transTokenContractContent = [{"constant":false,"inputs":[{"name":"receiver","type":"address"},{"name":"amount","type":"uint256"}],"name":"sendToken","outputs":[{"name":"sufficient","type":"bool"}],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[],"name":"getMyBalance","outputs":[{"name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[],"name":"symbol","outputs":[{"name":"","type":"string"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[{"name":"","type":"address"}],"name":"tokenBalanceOf","outputs":[{"name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[{"name":"act","type":"address"}],"name":"getBalance","outputs":[{"name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"inputs":[{"name":"_symbol","type":"string"},{"name":"supply","type":"uint256"}],"payable":false,"stateMutability":"nonpayable","type":"constructor"},{"anonymous":false,"inputs":[{"indexed":false,"name":"sender","type":"address"},{"indexed":false,"name":"receiver","type":"address"},{"indexed":false,"name":"amount","type":"uint256"}],"name":"TokenTransfer","type":"event"}];
const telcoMarketContractContent = [{"constant":true,"inputs":[],"name":"getTransTokenContractAddr","outputs":[{"name":"","type":"address"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[{"name":"addr","type":"address"}],"name":"setTransTokenContractAddr","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[],"name":"getMyTokenBalance","outputs":[{"name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[{"name":"itemId","type":"uint256"},{"name":"coinAmount","type":"uint256"}],"name":"buy","outputs":[{"name":"","type":"uint256"}],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":false,"inputs":[{"name":"itemId","type":"uint256"}],"name":"buy","outputs":[{"name":"","type":"uint256"}],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[{"name":"","type":"uint256"}],"name":"buyers","outputs":[{"name":"","type":"address"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[],"name":"getBuyers","outputs":[{"name":"","type":"address[8]"}],"payable":false,"stateMutability":"view","type":"function"}];

var Web3 = require('web3');
var web3 = new Web3();
console.log('web3:' + web3);

web3 = (typeof web3 !== 'undefined') 
    ? new Web3(web3.currentProvider) 
    : new Web3(new Web3.providers.HttpProvider(devnet_url));
web3.setProvider(new web3.providers.HttpProvider(devnet_url));

console.log('web3:' + web3 + ', devnet_url: ' + devnet_url);
console.log('web3.verion.api:' + web3.version.api);


function getWeb3() {
    return web3;
}

function getDeployedTransTokenContract() {
    const contract = web3.eth.contract(transTokenContractContent);
    return contract.at(transTokenContractAddr);
}

function getDeployedTelcoMarketContract() {
    const contract = web3.eth.contract(telcoMarketContractContent);
    return contract.at(telcoMarketContractAddr);
}
