pragma solidity ^0.4.0;

contract adder {
    string name;

    function setName(string _name) public {
        name = _name;
    }

    function getName() view public returns (string) {
        return name;
    }

    function add(int a, int b) pure public returns (int) {
        return a+b;
    }
}