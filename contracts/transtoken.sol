pragma solidity ^0.4.23;

contract TransToken { 
    mapping (address => uint) public coinBalanceOf;
    string public symbol;
    event CoinTransfer(address sender, address receiver, uint amount);
  
    /* Initializes contract with initial supply tokens to the creator of the contract */
    constructor(string _symbol, uint supply) public {
        symbol = _symbol;
        coinBalanceOf[msg.sender] = supply;
    }
  
    /* Very simple trade function */
    function sendCoin(address receiver, uint amount) public returns(bool sufficient) {
        if (coinBalanceOf[msg.sender] < amount) return false;
        coinBalanceOf[msg.sender] -= amount;
        coinBalanceOf[receiver] += amount;
        emit CoinTransfer(msg.sender, receiver, amount);
        return true;
    }
    
    function getMyBalance() view public returns (uint) {
        return coinBalanceOf[msg.sender];
    }
    function getBalance(address act) view public returns (uint) {
        return coinBalanceOf[act];
    }
}