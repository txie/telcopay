pragma solidity ^0.4.20;

contract TelcoPay {
    address public sender;
    address public receiver;
    uint public transferAmount;
    string public currencySymbol;
    uint constant transactionUpLimit = 50000;

    function TelcoPay() public {
        sender = msg.sender;
    }

    function getSenderBalance() view public returns (uint) {
        return msg.sender.balance;
    }

    // function transferMoney(uint transferAmount, string currencySymbol) public {
    //     sender = msg.sender;
    //     if (sender.balance >= transferAmount) {
    //         transferAmount = 100;
    //     }
    // }
    modifier onlySender() {
        require(msg.sender == sender, "only sender can call this");
        _;
    }

    modifier onlyReceiver() {
        require (msg.sender == receiver, "only receiver can call this");
        _;
    }

    // modifier canTransfer() {
    // }

    function confirmReceived() public onlyReceiver {
        sender.transfer(transferAmount);
    }


}